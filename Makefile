.PHONY: image container

image:
	docker build -t cookiecutter .

container:
	docker run -it --rm -d --name cookiecutter -v ~/dockvol:/dockvol cookiecutter