"""
testing utilities
"""

from pytest import mark
from typing import Tuple
from {{ cookiecutter.package_name }}.utils import add

@mark.parametrize("a,b,expected", [(0, 0, 0), (1, -1, 0), (1, 1, 2)])
def test_add(a: int, b: int, expected: int) -> None:
    assert add(a, b) == expected
