#!/bin/bash

# set default branch nae to main
git config --global init.defaultBranch main

# initialize as a git project
git init

# first commit
git add .
git commit -m "first commit"

# create the project on gitlab
git push --set-upstream git@gitlab.com:gautas/{{ cookiecutter.package_name }}.git main

# set up poetry
# poetry install -E test -E book

# push lock file
# git add poetry.lock
# git commit -m "adding poetry.lock"
# git push

# add a .env.container file
touch .env.container
